package kz.alfabank.creditsum;

import kz.alfabank.creditsum.ws.CalculateClient;
import kz.alfabank.creditsum.wsdl.CalcualteDateResponse;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import kz.alfabank.creditsum.wsdl.GetCalculateDateRq;
import kz.alfabank.creditsum.wsdl.GetCalculateDateRp;
import kz.alfabank.creditsum.wsdl.Params;
import org.springframework.ws.config.annotation.EnableWs;

@SpringBootApplication
@EnableWs
public class CreditsumApplication {

	public static void main(String[] args) {
		SpringApplication.run(CreditsumApplication.class, args);
	}
/*
	@Bean
	CommandLineRunner lookup(CalculateClient calculateClient) {
		return args -> {
			String valcode = "KZT";
			String amount = "200000";
			String shdtype = "AWITH";
			String startdate = "14.02.18";
			String payday = "25";
			String period = "MONTHS_12";
			String pcnval = "15";
			String pcncomm = "0.7";
			String dclcode = "14068";
			String inssum = "9000";

			CalcualteDateResponse response = calculateClient.calculateCreditSum(valcode,amount,shdtype,startdate,payday,period,pcnval,pcncomm,dclcode,inssum);
			System.err.println(response.toString());
		};
	}*/
}
