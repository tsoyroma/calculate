package kz.alfabank.creditsum.domain;

import java.math.BigDecimal;

public class CreditSum {
    private BigDecimal sum;
    private BigDecimal overpay;

    public CreditSum(BigDecimal sum, BigDecimal overpay) {
        this.sum = sum;
        this.overpay = overpay;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public BigDecimal getOverpay() {
        return overpay;
    }

    public void setOverpay(BigDecimal overpay) {
        this.overpay = overpay;
    }
}
