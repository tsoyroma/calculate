package kz.alfabank.creditsum.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CreditProduct {
    @JsonProperty("ATT_VALUE")
    private String attValue;
    @JsonProperty("ATT_NAME")
    private String attName;

    public String getAttValue() {
        return attValue;
    }

    public void setAttValue(String attValue) {
        this.attValue = attValue;
    }

    public String getAttName() {
        return attName;
    }

    public void setAttName(String attName) {
        this.attName = attName;
    }

    @Override
    public String toString() {
        return "Insurance{" +
                "att_value='" + attValue+ '\'' +
                ", att_name=" + attName +
                '}';
    }
}
