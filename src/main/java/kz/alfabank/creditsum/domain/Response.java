package kz.alfabank.creditsum.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Response {

    @JsonProperty("RETCODE")
    private Long retcode;
    @JsonProperty("RESPONSE")
    private List<CreditProduct> response;

    public  Response(){}

    public Long getRetcode() {
        return retcode;
    }

    public void setRetcode(Long retcode) {
        this.retcode = retcode;
    }

    public List<CreditProduct> getResponse() {
        return response;
    }

    public void setResponse(List<CreditProduct> response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return
                "RETCODE=" + retcode + response

                ;
    }
}