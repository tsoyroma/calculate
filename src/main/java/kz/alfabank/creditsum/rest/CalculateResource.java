package kz.alfabank.creditsum.rest;

import kz.alfabank.creditsum.domain.Response;
import kz.alfabank.creditsum.service.CalculateService;
import kz.alfabank.creditsum.service.CalculateServiceImpl;
import kz.alfabank.creditsum.service.CreditProductServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/calc")
public class CalculateResource {
    private static final Logger LOG = LoggerFactory.getLogger(CalculateResource.class);

    private static final String DEFAULT_PRODUCE_MEDIA_TYPE = MediaType.APPLICATION_JSON_VALUE;

    private final CalculateServiceImpl calculateService;

    @Autowired
    public  CalculateResource(CalculateServiceImpl calculateService){
        this.calculateService=calculateService;
    }

    @GetMapping(value = "/credit", produces = DEFAULT_PRODUCE_MEDIA_TYPE)
    public String getAtts(@RequestParam(value = "code",required = true) String code) {

        return calculateService.getCreditSum("200000","dsa","2");
    }
}

/*
@RestController
@RequestMapping("/calc")
public class InsCalcResource {

    private static final Logger LOG = LoggerFactory.getLogger(InsCalcResource.class);

    private static final String DEFAULT_PRODUCE_MEDIA_TYPE = MediaType.APPLICATION_JSON_VALUE;

    private final InsuranceServiceImpl insuranceService;

    @Autowired
    public InsCalcResource(InsuranceServiceImpl service) {
        this.insuranceService = service;
    }


    @GetMapping(value = "/insurance", produces = DEFAULT_PRODUCE_MEDIA_TYPE)
    public ResponseEntity<InsuranceSum> getInsuranceSum(@RequestParam(value = "code",required = true) String code, @RequestParam(value = "sum",required = true) BigDecimal sum) {
        LOG.debug(new StringBuilder().append("REST request InsuranceSum ").toString());
        return ResponseEntity.ok(insuranceService.getByCode(code,sum));
    }

}
 */