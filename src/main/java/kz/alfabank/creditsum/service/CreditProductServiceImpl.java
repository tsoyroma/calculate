package kz.alfabank.creditsum.service;

import kz.alfabank.creditsum.domain.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class CreditProductServiceImpl implements CreditProductService {

    private static final Logger LOG = LoggerFactory.getLogger(CreditProductServiceImpl.class);

    @Autowired
    RestTemplate restTemplate;

    @Value("${kz.alfabank.dictapi}")
    private String url;

    public Response getAttByCode(String code){
      ResponseEntity<Response> responseEntity = restTemplate.getForEntity(url + "/" + code,Response.class);
        return responseEntity.getBody();
    }

}
