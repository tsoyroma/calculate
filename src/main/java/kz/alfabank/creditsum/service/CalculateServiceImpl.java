package kz.alfabank.creditsum.service;

import kz.alfabank.creditsum.domain.CreditProduct;
import kz.alfabank.creditsum.domain.CreditSum;
import kz.alfabank.creditsum.domain.Response;
import kz.alfabank.creditsum.ws.CalculateClient;
import kz.alfabank.creditsum.wsdl.CalcualteDateResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class CalculateServiceImpl implements CalculateService {

    private static final Logger LOG = LoggerFactory.getLogger(CalculateServiceImpl.class);

    private final CreditProductServiceImpl creditProductService;

   private CalculateClient calculateClient;

    @Autowired
    public CalculateServiceImpl(CreditProductServiceImpl service) {
        this.creditProductService = service;
    }

    public String getCreditSum(String amount, String shdtype, String pcode) {
        //public CreditSum getCreditSum(String amount, String shdtype, String pcode) {
        Response res = creditProductService.getAttByCode(pcode);
        List<CreditProduct> listatts = res.getResponse();
        List<CreditProduct> colvircodelist = listatts.stream()
                .filter(a -> Objects.equals(a.getAttName(), "COLVIR_CODE"))
                .collect(Collectors.toList());
        List<CreditProduct> ratelist = listatts.stream()
                .filter(a -> Objects.equals(a.getAttName(), "RATE"))
                .collect(Collectors.toList());
        BigDecimal rate = new BigDecimal(ratelist.get(0).getAttValue());
        String colvircode = colvircodelist.get(0).getAttValue();
        CalcualteDateResponse calcualteDateResponse= calculateClient.calculateCreditSum("KZT",amount,"AWITH","14.02.2018","25","MONTHS_12",rate.toString(),"0.7","14068","9000");

        return calcualteDateResponse.toString();
    }
}