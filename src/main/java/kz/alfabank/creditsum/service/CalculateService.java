package kz.alfabank.creditsum.service;

import kz.alfabank.creditsum.domain.CreditSum;

import java.math.BigDecimal;
import java.time.LocalDate;

public interface CalculateService {
    //CreditSum getCreditSum(String amount, String shdtype, String pcode);
    String getCreditSum(String amount, String shdtype, String pcode);
}
