package kz.alfabank.creditsum.service;

import kz.alfabank.creditsum.domain.Response;

public interface CreditProductService {
    Response getAttByCode(String code);
}
