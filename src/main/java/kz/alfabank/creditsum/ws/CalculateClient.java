package kz.alfabank.creditsum.ws;
import kz.alfabank.creditsum.wsdl.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.SoapVersion;
import org.springframework.ws.soap.client.core.SoapActionCallback;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;

import java.util.ArrayList;
import java.util.List;
@Component
public class CalculateClient{

    @Autowired
    private WebServiceTemplate webServiceTemplate;

    private static final Logger log = LoggerFactory.getLogger(CalculateClient.class);
    @Value("${kz.alfabank.serviceuri}")
    private String uri;

    public CalcualteDateResponse calculateCreditSum(String valcode, String amount, String shdtype, String startdate, String payday, String period, String pcnval, String pcncomm, String dclcode, String inssum){
        ObjectFactory objectFactory = new ObjectFactory();
        CalcualteDate rootreq = objectFactory.createCalcualteDate();;
        GetCalculateDateRq req = objectFactory.createGetCalculateDateRq();
        req.setClientDepId("");
        req.setClientId("");
        req.setReqid("5395");
        req.setSystemcode("5395");
        req.setSystemuser("5395");
        req.setDebug("5395");

        Params params = objectFactory.createParams();
        List<Params.Param> paramList = new ArrayList<Params.Param>();
        paramList.add(new Params.Param("VALCODE",valcode));
        paramList.add(new Params.Param("AMOUNT",amount));
        paramList.add(new Params.Param("SHDTYPE",shdtype));
        paramList.add(new Params.Param("STARTDATE",startdate));
        paramList.add(new Params.Param("PAYDAY",payday));
        paramList.add(new Params.Param("PERIOD",period));
        paramList.add(new Params.Param("PCNVAL",pcnval));
        paramList.add(new Params.Param("PCNCOMM",pcncomm));
        paramList.add(new Params.Param("DCLCODE",dclcode));
        paramList.add(new Params.Param("INSURANCESUM",inssum));
        params.setParam(paramList);
        req.setParams(params);
        rootreq.setGetCalculateDateRq(req);

        log.info("Some log");
        javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
                new javax.net.ssl.HostnameVerifier(){

                    public boolean verify(String hostname,
                                          javax.net.ssl.SSLSession sslSession) {
                        return hostname.equals("vserver266");
                    }
                });
        CalcualteDateResponse res = (CalcualteDateResponse)webServiceTemplate.marshalSendAndReceive(uri,rootreq);
        return res;
    }

}